import React from 'react'
import {Link} from 'react-router-dom'

export const Sidebar = () => {
  return (
    <div className="sidebar-wrapper">
      <nav id="sidebar">
        <ul className="list-unstyled components">
          <li>
            <Link to="/Dashboard"><i className='fa fa-tachometer'></i> Administración         
            </Link>
          </li>
          {/*Botones de producto*/}
          <li>
            <a href= "#productSubmenu" data-toggle="collapse" aria-expanded="false" className='dropdown-toggle'><i className='fa fa-product-hunt'></i> Productos</a>
            <ul className="collapse list-unstyled" id="productSubmenu">
              <li>
                <Link to="/ProductList"><i className="fa fa-clipboard"></i>Lista de productos</Link>
              </li>
              <li>
                <Link to="/nuevoProducto"><i className="fa fa-plus"></i>Crear producto</Link>
              </li>
            </ul>
          </li>
          
          {/*Botones de pedidos*/}
          <li>
            <Link to="/admin/orders"><i className="fa fa-shopping-basket"></i>Pedidos</Link>
          </li>
           {/*Botones de usuario*/}
          <li>
            <Link to="/admin/users"><i className="fa fa-users"></i>Usuarios</Link>
          </li>
          {/*Botones de reviews*/}
          <li>
            <Link to="/admin/reviews"><i className="fa fa-users"></i>Reviews</Link>
          </li>

        </ul>
      </nav>
    </div>
  )
}

export default Sidebar