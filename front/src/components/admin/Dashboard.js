import React, { Fragment } from 'react'
import Sidebar from './Sidebar'
import MetaData from '../layout/MetaData'
import {Link} from 'react-router-dom'

export const Dashboard = () => {
  return (
    <Fragment>
      <div className='row'>
        <div className='col-12 col-md-2'>
          <Sidebar />
        </div>
        <div className='col-12 col-md-10'><h1 className='my-4'>Dashboard</h1>
          <Fragment>
            <MetaData title={"Administración"} />
            <div className='row pr-4'>
              {/*Targeta 1 */}
              <div className='col-xl-12 col-sm-12 mb-3'>
                <div className='card text-white bg-primary o-hidden h-100'>
                  <div className='card-body'>
                    <div className='text-center card-font-size'>Ventas totales<br /><b>$25.000.000</b>
                    </div>
                  </div>
                </div>
            </div>
            {/*Targeta 2 */}
              <div className='col-xl-3 col-sm-6 mb-3'>
                <div className='card text-white bg-success o-hidden h-100'>
                  <div className='card-body'>
                    <div className='text-center card-font-size'>Productos<br /><b>250</b>
                    </div></div>
                    <Link className="card-footer text-white clearfix small z-1" to="/Home">
                      <span className='flaot-left'>Ver detalles</span>
                      <span className='float-right'><i className='fa fa-angle-right'></i></span>
                    </Link>
                </div>
            </div>
            {/*Targeta 3 */}
              <div className='col-xl-3 col-sm-6 mb-3'>
                <div className='card text-white bg-warning o-hidden h-100'>
                  <div className='card-body'>
                    <div className='text-center card-font-size'>Pedidos<br /><b>70</b>
                    </div></div>
                    <Link className="card-footer text-white clearfix small z-1" to="/admin/orders">
                      <span className='flaot-left'>Ver detalles</span>
                      <span className='float-right'><i className='fa fa-angle-right'></i></span>
                    </Link>
                  </div>
                </div>
              
            {/*Targeta 4 */}
              <div className='col-xl-3 col-sm-6 mb-3'>
                <div className='card text-white bg-dark o-hidden h-100'>
                  <div className='card-body'>
                    <div className='text-center card-font-size'>Usuarios<br /><b>310</b>
                    </div></div>
                    <Link className="card-footer text-white clearfix small z-1" to="/admin/users">
                      <span className='float-left'>Ver detalles</span>
                      <span className='float-right'><i className='fa fa-angle-right'></i></span>
                    </Link>
                  </div>
                </div>
            
          
            {/*Targeta 5 */}

              <div className='col-xl-3 col-sm-6 mb-3'>
                <div className='card text-white bg-danger o-hidden h-100'>
                  <div className='card-body'>
                    <div className='text-center card-font-size'>Productos agotados<br /><b>50</b>
                    </div>
                  </div>
                </div>
            </div>
          </div>
          </Fragment>
        </div>
      </div>
    </Fragment>
  )
}
