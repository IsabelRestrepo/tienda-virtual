const catchAsyncErrors = require("../middleware/catchAsyncErrors");
const producto=require("../models/Productos");
const ErrorHandler = require("../utils/errorHandler");
const fetch=(url)=>import('node-fetch').then(({default:fetch})=>fetch(url));
//Ver la lista de productos
exports.getProducts=async(req,res,next)=>{
  const productos=await producto.find();
  if(!productos){
    return res.status(404).json({
      success:false,
      error:true
    })
  }
  res.status(200).json({
    success:true,
    cantidad:productos.length,
    productos
  })
}

// Encontrar un producto por id

exports.getProduct=catchAsyncErrors( async(req,res,next)=>{
  const product=await producto.findById(req.params.id);
  if (!product){
    return next (new ErrorHandler("producto no encontrado"),404)
  }
  res.status(200).json({
    success:true,
    message: 'Aqui encuentras infromación sobre el producto',
    product
  })
})
// Crear un nuevo producto /api/productos
exports.newProduct=catchAsyncErrors( async(req,res,next)=>{
const product=await producto.create(req.body);

res.status(201).json({
  success:true,
  product
})
})

// Actualizar productos

exports.updateProduct=catchAsyncErrors( async (req,res,next)=>{
  let product=await producto.findById(req.params.id);
  if (!product){// Verifica la existencia de un producto
    return next (new ErrorHandler("producto no encontrado"),404)
  }
// si existe el objeto ejecuta la actualización
  product=await producto.findByIdAndUpdate(req.params.id,req.body,{
    new:true,
    runValidators:true

  });
  // responde Ok cuando se actualiza
  res.status(200).json({
    success:true,
    message:"Producto actualizado correctamente",
    product
  })
})
 // Eliminar un producto
 exports.deleteProduct=catchAsyncErrors( async(req,res,next)=>{
  const product=await producto.findById(req.params.id);
  if(!product){
    return next (new ErrorHandler("producto no encontrado"),404)
  }
  await product.remove();
    res.status(200).json({
      success:true,
      message:"Producto eliminado correctamente"
    })
})

//Implementación de fetch
// Ver todos los productos, Ver por id
function verProductos(){
  fetch('http://localhost:4000/api/productos')
  .then(res=>res.json()
  .then(res=> console.log(res)))
  .catch(err=>console.error(err))
}
//verProductos(); LLamamos al método creado para probar la consulta

//Ver por id 
function verProductoPorId(id){
  fetch('http://localhost:4000/api/productos/verProducto/'+id)
  .then(res=>res.json()
  .then(res=> console.log(res)))
  .catch(err=>console.error(err))
 
}
//verProductoPorId('63460460fd3d979c5ab22b06') //llamamos al metodo para probar la funcion creada 