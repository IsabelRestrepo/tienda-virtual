const User= require ("../models/auth")
const ErrorHandler= require("../middleware/catchAsyncErrors")
const catchAsyncErrors = require("../middleware/catchAsyncErrors")

exports.registroUsuario=catchAsyncErrors(async (req,res,next)=>{
  const {nombre,email,password}=req.body;
  const user= await User.create({
    nombre,
    email,
    password,
    avatar:{
      public_id:"4567",
      url:"https://www.google.com/url?sa=i&url=https%3A%2F%2Fpixabay.com%2Fes%2Fvectors%2Fusuario-avatar-icono-de-usuario-6380868%2F&psig=AOvVaw3FHM6E7LAujnYT9eDytw5d&ust=1667132112641000&source=images&cd=vfe&ved=0CAwQjRxqFwoTCJjy6py1hfsCFQAAAAAdAAAAABAE"}
    })

res.status(201).json({
  success:true,
  user
})
})