const express=require("express")
const router=express.Router();

const{getProducts, newProduct,getProduct,updateProduct,deleteProduct}=require("../controller/productsController");

router.route('/producto').get(getProducts); //establecemos la ruta desde la que deseamos ver los productos
router.route('/producto/nuevo').post(newProduct);
router.route('/producto/:id').get(getProduct)
router.route('/producto/:id').put(updateProduct)
router.route('/producto/:id').delete(deleteProduct)

module.exports=router;